import { NgModule } from '@angular/core';

import { SharedModule } from "../common/shared.module";
import { contactRouter } from './contact.router';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [         
    ContactPageComponent,
    ContactComponent
     ],
    providers: [],
  imports: [ contactRouter, SharedModule ]
})

export class ContactModule {}