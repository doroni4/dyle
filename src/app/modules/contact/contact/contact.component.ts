import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Http, Response, Jsonp, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { Email } from '../../../../models/email';
import { NgForm } from '@angular/forms';
import * as $ from "jquery";
import { ProductSeries } from '../../../../enums/product-series.enum';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'my-contact',
  templateUrl: 'contact.component.html',
  styles: []
})
export class ContactComponent implements OnInit {
  @ViewChild('contactForm') public contactForm: NgForm;
  public model: Email;
  public series: ProductSeries;

  constructor(private route: ActivatedRoute, private http: Http) { }

  ngOnInit() {
    this.model = new Email();
    this.route.data.subscribe((data: { series: ProductSeries }) => {
      if(!data.series){
        return;
      }
      this.series = data.series;
      
      this.model.message = "Please send to me model " + ProductSeries[this.series] + " marketing materials.";

      // In a real app: dispatch action to load the details here.
   });
    
  }

  sendEmail() {

    var cfResponse = $('#contactFormResponse');
    var cfsubmit = $("#cfsubmit");
    var cfsubmitText = cfsubmit.text();

    cfsubmit.text("Sending...");

    this.http.post('assets/php/contact.php', JSON.stringify(this.model))
      .subscribe(res => {
        cfResponse.html(res.toString());
        cfsubmit.text(cfsubmitText);
        this.contactForm.reset();

        console.log(res);
      },
      err => {
        cfsubmit.text('Something bad happend during sending this message. Please try again later.');
      console.log(err);
      });
  }

}
