import { Routes, RouterModule } from '@angular/router';
import { ContactPageComponent } from './contact-page/contact-page.component';

const CONTACT_ROUTER: Routes = [
    { 
        path: '',
        component: ContactPageComponent        
    }
];

export const contactRouter = RouterModule.forChild(CONTACT_ROUTER);

