import { Component, OnInit, Inject } from '@angular/core';
import { LOCALE_ID, PLATFORM_ID } from '@angular/core';

@Component({
  selector: 'my-brochures',
  templateUrl: 'brochures.component.html'
})

export class BrochuresComponent implements OnInit {

  constructor( @Inject(LOCALE_ID) public currentLocale: string, ) { }

  ngOnInit() {
  }

}
