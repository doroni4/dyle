import { Component, OnInit } from '@angular/core';
import { ProductSelectorService } from '../../../../services/product-selector.service';
import { Product } from '../../../../models/product';
import { ProductModel } from '../../../../enums/product-model.enum';

@Component({
  selector: 'my-personalization',
  templateUrl: 'personalize.component.html',
  styles: []
})
export class PersonalizeComponent implements OnInit {
  selectedProduct:Product;
  personalizeImage:string;
  constructor(private productSelectionService: ProductSelectorService) { }

  ngOnInit() {
    this.productSelectionService.selectedProduct.subscribe((product) => {
      this.selectedProduct = product;      
      
      let model = product.model ? product.model : ProductModel.black_oak;
      this.personalizeImage = 'assets/images/configurator/headers/model-' + model + '.jpg';
    });
  }

}
