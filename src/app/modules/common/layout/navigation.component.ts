import { Component, OnInit, Inject } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { environment } from '../../../../environments/environment';
@Component({
  selector: 'my-navigation',
  templateUrl: './navigation.component.html'
})
export class NavigationComponent implements OnInit {
  public englishSiteUrl: string;
  public polishSiteUrl: string;
  public germanSiteUrl: string;
  constructor(@Inject(LOCALE_ID) public currentLocale: string,) {
    // Do stuff
    
  }
  ngOnInit() {
    this.englishSiteUrl = environment.englishAppUrl;
    this.polishSiteUrl = environment.polishAppUrl;
    this.germanSiteUrl = environment.germanAppUrl;
  }
}
