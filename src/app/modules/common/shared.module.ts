import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NavigationComponent } from "./layout/navigation.component";
import { PersonalizeComponent } from "./personalize/personalize.component";
import { FamilyComponent } from "./family/family.component";
import { BrochuresComponent } from "./brochures/brochures.component";
import { ServicesComponent } from "./services/services.component";
import { FooterComponent } from "./footer/footer.component";
import { SitemapComponent } from "./sitemap/sitemap.component";

import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { GlobalActionsHelper } from "../../shared/global-actions.helper";

import { ProductSelectorService } from "../../../services/product-selector.service";
import { ProductResolverService } from "../../../services/product-resolver.service";
import { SeriesResolverService } from '../../../services/series-resolver.service';
import { NguCarouselModule } from '@ngu/carousel';
import { CurrencyService } from '../../../services/currency.service';

@NgModule({
  declarations: [
    NavigationComponent,
    PersonalizeComponent,
    FamilyComponent,
    BrochuresComponent,
    ServicesComponent,
    FooterComponent,
    SitemapComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    NguCarouselModule
  ],
  providers: [
    GlobalActionsHelper,
    ProductResolverService,
    ProductSelectorService,
    CurrencyService,
    SeriesResolverService
  ],
  exports: [
    NavigationComponent,
    PersonalizeComponent,
    FamilyComponent,
    BrochuresComponent,
    ServicesComponent,
    FooterComponent,
    FormsModule,
    CommonModule,
    SitemapComponent,
    NguCarouselModule
  ]
})

export class SharedModule { }