import { Routes, RouterModule } from '@angular/router';
import { CooperationPageComponent } from './cooperation-page/cooperation-page.component';

const COOPERATION_ROUTER: Routes = [
    { 
        path: '',
        component: CooperationPageComponent        
    }
];

export const cooperationRouter = RouterModule.forChild(COOPERATION_ROUTER);

