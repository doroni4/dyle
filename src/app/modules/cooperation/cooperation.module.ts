import { NgModule } from '@angular/core';

import { SharedModule } from "../common/shared.module";
import { cooperationRouter } from './cooperation.router';
import { CooperationPageComponent } from './cooperation-page/cooperation-page.component';
import { CooperationParallaxComponent } from './cooperation-parallax/cooperation-parallax.component';



@NgModule({
  declarations: [         
    CooperationPageComponent,
    CooperationParallaxComponent
     ],
    providers: [],
  imports: [ cooperationRouter, SharedModule ]
})

export class CooperationModule {}