import { Component, OnInit, ViewChild } from '@angular/core';
import { Email } from '../../../../models/email';
import * as $ from "jquery";
import { NgForm } from '@angular/forms';
import { Http } from '@angular/http';

@Component({
  selector: 'app-cooperation-page',
  templateUrl: './cooperation-page.component.html',
  styles: []
})
export class CooperationPageComponent implements OnInit {
  @ViewChild('requestForm') public requestForm: NgForm;
  public model: Email;
  public currentSeries: string;

  constructor(private http: Http) { }

  ngOnInit() {
    this.model = new Email();
  }

  setSeries(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    this.currentSeries = target.dataset.series;
  }
  sendMaterialsRequest() {

    var cfResponse = $('#requestFormResponse');
    var rfsubmit = $("#rfsubmit");
    var rfsubmitText = rfsubmit.text();

    rfsubmit.text("Sending...");

    this.model.message = "Please send to me model " + this.currentSeries + " marketing materials.";

    this.http.post('/assets/php/request.php', JSON.stringify(this.model))
      .subscribe(res => {
        cfResponse.html("Request has been sent.");
        rfsubmit.text(rfsubmitText);
        this.requestForm.reset();

        console.log(res);
      },
      err => {
        rfsubmit.text('Something bad happend during sending this message. Please try again later.');
        console.log(err);
      });
  }
}
