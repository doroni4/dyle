import { NgModule } from '@angular/core';

import { SharedModule } from "../common/shared.module";
import { homeRouter } from './home.router';
import { HomeComponent} from './home/home.component'
import { WelcomeScreenComponent} from './welcome-screen/welcome-screen.component'
import { ConceptComponent} from './concept/concept.component'
import { ProductsComponent} from './products/products.component'
import { AboutComponent} from './about/about.component'
import { VisitUsComponent} from './visit-us/visit-us.component'

@NgModule({
  declarations: [         
    HomeComponent,
    AboutComponent,
    WelcomeScreenComponent,
    ConceptComponent,   
    ProductsComponent, 
    VisitUsComponent,

     ],
    providers: [],
  imports: [ homeRouter, SharedModule ]
})

export class HomeModule {}