import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

declare var $: any;

@Component({
  selector: 'home',  
  templateUrl: 'home.component.html'
})
export class HomeComponent implements OnInit,AfterViewInit {
  public message: string;
  public userCity: string;
  constructor() {}

  ngAfterViewInit(){
    // $('.loader').fadeOut();
    // $('.page-loader').delay(350).fadeOut('slow');
  }
  ngOnInit() {
    this.message = 'Hello';
    this.userCity = "";    
    // this.geoService.findLocation().subscribe(data => {
    //   this.userCity = data.city;
    // });
  }

  //<div  *ngFor="let item of results"> ... </div>
}