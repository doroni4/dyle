import { Component, OnInit } from '@angular/core';
import { ProductSizes } from "../../../../enums/product-sizes.enum";
import { ProductTops } from "../../../../enums/product-tops.enum";
import { ProductLegs } from "../../../../enums/product-legs.enum";
import { ProductSeries } from "../../../../enums/product-series.enum";
import { ProductModel } from "../../../../enums/product-model.enum";
import { Product } from "../../../../models/product";
import { ProductSelectorService } from "../../../../services/product-selector.service";


@Component({
  selector: 'my-product-description',
  templateUrl: './product-description.component.html',
  styles: []
})
export class ProductDescriptionComponent implements OnInit {
  constructor(private productSelectionService: ProductSelectorService) {}
  ProductSizes : typeof ProductSizes = ProductSizes;
  ProductTops : typeof ProductTops = ProductTops;
  ProductLegs : typeof ProductLegs = ProductLegs;
  ProductSeries: typeof ProductSeries = ProductSeries;
  ProductModel: typeof ProductModel = ProductModel;
  
  selectedProduct:Product;

  ngOnInit() {
    this.productSelectionService.selectedProduct.subscribe((product) => {
      this.selectedProduct = product;
    });
  }

}
