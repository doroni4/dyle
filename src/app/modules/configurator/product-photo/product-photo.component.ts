import { Component, OnInit, PLATFORM_ID, Inject } from '@angular/core';
import { ProductSelectorService } from "../../../../services/product-selector.service";
import { Product } from '../../../../models/product';
import { ProductSizes } from '../../../../enums/product-sizes.enum';
import { ProductTops } from '../../../../enums/product-tops.enum';
import { ProductLegs } from '../../../../enums/product-legs.enum';
import { ProductSeries } from '../../../../enums/product-series.enum';
import { ProductModel } from '../../../../enums/product-model.enum';
import { NguCarousel } from '@ngu/carousel';
import { isPlatformBrowser } from '@angular/common';
import { AfterViewInit } from '@angular/core/src/metadata/lifecycle_hooks';
declare var $: any;

@Component({
  selector: 'my-product-photo',
  templateUrl: './product-photo.component.html',
  styleUrls: ['./product-photo.scss']
})
export class ProductPhotoComponent implements OnInit {
  constructor( @Inject(PLATFORM_ID) public platformId: Object, private productSelectionService: ProductSelectorService) { }
  ProductSizes: typeof ProductSizes = ProductSizes;
  ProductTops: typeof ProductTops = ProductTops;
  ProductLegs: typeof ProductLegs = ProductLegs;
  ProductSeries: typeof ProductSeries = ProductSeries;
  ProductModel: typeof ProductModel = ProductModel;
  public carouselTile: NguCarousel;

  public carouselTileOneItems: Array<any> = [];
  public carouselTileOne: NguCarousel;

  public loading: Boolean = true;
  selectedProduct: Product = new Product();
  imageName: string;
  hasAlternateImages: boolean;
  altImagesDirName: string;
  onLoad() {
    this.loading = false;
}
  ngOnInit() {
    this.carouselTileOne = {
      grid: { xs: 2, sm: 3, md: 4, lg: 6, all: 0 },
      speed: 600,
      interval: 2000,
      point: {
        visible: false
      },
      load: 2,
      loop: false,
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };

    this.productSelectionService.selectedProduct.subscribe((product) => {
      this.selectedProduct = product;

      this.altImagesDirName =
        ProductModel[product.model] +
        '_' +
        ProductTops[product.top] +
        '_' +
        ProductLegs[product.legs];

      this.imageName = 'assets/images/configurator/' + this.altImagesDirName + '_' +
        ProductSizes[product.size];

      this.carouselTileOneItems = [];
      this.hasAlternateImages = this.selectedProduct.altImagesQty > 0;

      if (!this.hasAlternateImages) {
        this.imageName += '.jpg';
        this.bindZooming();
        return;
      }
      this.carouselTileOneItems.push(this.imageName)
      this.imageName += '.jpg';
      var multiplier = this.selectedProduct.legs == ProductLegs.black ? 2 : 1;
      for (var i = 0; i < this.selectedProduct.altImagesQty * multiplier; i++) {
        this.carouselTileOneItems.push('assets/images/configurator/' + this.altImagesDirName + '/00' + (i + 1));
      }
      this.bindZooming();
    });
  }

  public changeImage(imagePath: string) {
    this.loading = true;
    this.imageName = this.getImagePath(imagePath);
    this.bindZooming();
  }

  public getImagePath(tile: string) {
    return tile + '.jpg';
  }

  public getMinImagePath(tile: string) {
    return tile + '_min.jpg';
  }

  public isSamePath(imgPath: string) {
    return this.getImagePath(imgPath) == this.imageName;
  }

  public bindZooming() {

    if (isPlatformBrowser(this.platformId)) {
      var items = [];
      var startIndex = this.carouselTileOneItems.findIndex(this.isSamePath, this);
      var elementsNo = this.carouselTileOneItems.length;
      for (var i = 0; i < elementsNo; i++) {
        items.push({
          src: this.getImagePath(this.carouselTileOneItems[(i + startIndex) % elementsNo])
        });
      }


      $('.main-image').magnificPopup({
        items: items,
        type: 'image',
        gallery: {
          enabled: true
        },
      });

    }
  };
}
