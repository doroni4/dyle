import { Component, OnInit, AfterViewInit, Inject, PLATFORM_ID } from '@angular/core';
import { NguCarousel } from '@ngu/carousel';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

declare var $:any;

@Component({
  selector: 'my-product-gallery',
  templateUrl: './product-gallery.component.html',
  styleUrls: ['product-gallery.scss']
})
export class ProductGalleryComponent implements OnInit, AfterViewInit {
  public carouselTileItems: Array<any>;
  public carouselTile: NguCarousel;

  public carouselTileOneItems: Array<any> = [
      
    'assets/images/dyle-gallery/dyle-gal (13).jpg',
    'assets/images/dyle-gallery/dyle-gal (15).jpg',
    'assets/images/dyle-gallery/dyle-gal (17).jpg',
    'assets/images/dyle-gallery/dyle-gal (18).jpg',
    'assets/images/dyle-gallery/dyle-gal (20).jpg',
    'assets/images/dyle-gallery/dyle-gal (22).jpg',
    'assets/images/dyle-gallery/dyle-gal (23).jpg',
    'assets/images/dyle-gallery/dyle-gal (25).jpg',
    'assets/images/dyle-gallery/dyle-gal (26).jpg',
    'assets/images/dyle-gallery/dyle-gal (28).jpg',
    'assets/images/dyle-gallery/dyle-gal (30).jpg',
    'assets/images/dyle-gallery/dyle-gal (32).jpg',
    'assets/images/dyle-gallery/dyle-gal (34).jpg',
    'assets/images/dyle-gallery/dyle-gal (1).JPG',
    'assets/images/dyle-gallery/dyle-gal (2).JPG',
    'assets/images/dyle-gallery/dyle-gal (3).JPG',
    'assets/images/dyle-gallery/dyle-gal (4).JPG',
    'assets/images/dyle-gallery/dyle-gal (5).JPG',
    'assets/images/dyle-gallery/dyle-gal (6).JPG',
    'assets/images/dyle-gallery/dyle-gal (7).JPG',
    'assets/images/dyle-gallery/dyle-gal (8).JPG',
    'assets/images/dyle-gallery/dyle-gal (9).JPG',
    'assets/images/dyle-gallery/dyle-gal (10).JPG',
    'assets/images/dyle-gallery/dyle-gal (11).JPG'
  ];
  public carouselTileOne: NguCarousel;

  constructor(@Inject(PLATFORM_ID) public platformId: Object){
    
  }
  ngOnInit(){
    this.carouselTileOne = {
      grid: { xs: 2, sm: 3, md: 4, lg: 6, all: 0 },
      speed: 600,
      interval: 3000,
      point: {
        visible: true,
        pointStyles: `
          .ngucarouselPoint {
            list-style-type: none;
            text-align: center;
            padding: 12px;
            margin: 0;
            white-space: nowrap;
            overflow: auto;
            box-sizing: border-box;
          }
          .ngucarouselPoint li {
            display: inline-block;
            border-radius: 50%;
            background: #6b6b6b;
            padding: 5px;
            margin: 0 3px;
            transition: .4s;
          }
          .ngucarouselPoint li.active {
              border: 2px solid rgba(0, 0, 0, 0.55);
              transform: scale(1.2);
              background: transparent;
            }
        `
      },
      load: 2,
      loop: true,
      touch: true,
      easing: 'ease',
      animation: 'lazy'
    };
    
    
  }

  ngAfterViewInit(){
    if(isPlatformBrowser(this.platformId)){
      $(".gallery-item").magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        },
        image: {
            titleSrc: 'title',
            tError: 'The image could not be loaded.'
        }
    });
    }
  };
}