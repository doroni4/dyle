import { NgModule } from '@angular/core';
import { ProductOptionsComponent } from './product-options/product-options.component';
import { ProductDescriptionComponent } from './product-description/product-description.component';
import { ProductPriceComponent } from './product-price/product-price.component';
import { ProductPhotoComponent } from './product-photo/product-photo.component';
import { ProductHeaderComponent } from './product-header/product-header.component';
import { ProductGalleryComponent } from './product-gallery/product-gallery.component';
import { ConfiguratorComponent } from "./configurator-page/configurator.component";

import { ProdutConfiguratorComponent } from "./produt-configurator/produt-configurator.component";
import { configuratorRouter } from "./configurator.router";
import { SharedModule } from "../common/shared.module";
import { DyleCurrencyPipe } from "../../shared/dyle-currency.pipe";
import { ProductSelectorService } from "../../../services/product-selector.service";

@NgModule({
  declarations: [         
        ProductOptionsComponent,
        ProductDescriptionComponent,
        ProductPriceComponent,
        ProductPhotoComponent,
        ProductHeaderComponent,
        ProdutConfiguratorComponent,
        ProductGalleryComponent,
        ConfiguratorComponent,
        DyleCurrencyPipe
     ],
    providers: [],
  imports: [ configuratorRouter, SharedModule ]
})

export class ConfiguratorModule {}