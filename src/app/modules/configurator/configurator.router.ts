import { Routes, RouterModule } from '@angular/router';
import { ConfiguratorComponent } from "./configurator-page/configurator.component";
import { ProductResolverService } from "../../../services/product-resolver.service";

const CONFIGURATOR_ROUTER: Routes = [
    { 
        path: '',
        component: ConfiguratorComponent        
    }
];

export const configuratorRouter = RouterModule.forChild(CONFIGURATOR_ROUTER);

