import { Component, OnInit } from '@angular/core';
import { ProductSelectorService } from "../../../../services/product-selector.service";
import { Product } from '../../../../models/product';
import { ProductSizes } from '../../../../enums/product-sizes.enum';
import { ProductTops } from '../../../../enums/product-tops.enum';
import { ProductLegs } from '../../../../enums/product-legs.enum';
import { ProductSeries } from '../../../../enums/product-series.enum';
import { ProductModel } from '../../../../enums/product-model.enum';

@Component({
  selector: 'my-product-header',
  templateUrl: './product-header.component.html',
  styles: []
})
export class ProductHeaderComponent implements OnInit {
  constructor(private productSelectionService: ProductSelectorService) {}
  ProductSizes : typeof ProductSizes = ProductSizes;
  ProductTops : typeof ProductTops = ProductTops;
  ProductLegs : typeof ProductLegs = ProductLegs;
  ProductSeries: typeof ProductSeries = ProductSeries;
  ProductModel: typeof ProductModel = ProductModel;

  selectedProduct:Product;

  ngOnInit() {
    this.productSelectionService.selectedProduct.subscribe((product) => {
      this.selectedProduct = product;
    });
  }

}
