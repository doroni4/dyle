import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GroupProduct } from '../../../../models/groupProduct';
import { ProductSelectorService } from "../../../../services/product-selector.service";
import { CurrencyService } from '../../../../services/currency.service';
@Component({
  selector: 'my-produt-configurator',
  templateUrl: './produt-configurator.component.html',
  styles: []
})
export class ProdutConfiguratorComponent implements OnInit {
  public groupProduct: GroupProduct;

  constructor(private route: ActivatedRoute,
    public currencyService: CurrencyService) { }

  ngOnInit() {
    this.route.data.subscribe((data: { product: GroupProduct }) => {
      this.groupProduct = data.product;

      // In a real app: dispatch action to load the details here.
    });
  }

}
