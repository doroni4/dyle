import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ProductSelectorService } from "../../../../services/product-selector.service";
import { Product } from "../../../../models/product";
import { ProductSeries } from "../../../../enums/product-series.enum";
import { ProductLegs } from "../../../../enums/product-legs.enum";
import { ProductSizes } from '../../../../enums/product-sizes.enum';
import { ProductTops } from '../../../../enums/product-tops.enum';
import { GroupProduct } from "../../../../models/groupProduct";

@Component({
  selector: 'my-product-options',
  templateUrl: './product-options.component.html',
  styles: []
})
export class ProductOptionsComponent implements OnInit {
  @Input() groupProduct: GroupProduct;

  public showSizes: boolean = true;
  public showLegs: boolean = true;
  public showTop: boolean = true;

  public hasSize200x100: boolean = false;
  public hasSize240x100: boolean = false;

  public hasBlackLegs: boolean = false;
  public hasSilverLegs: boolean = false;
  public hasGoldLegs: boolean = false;

  public hasNormalOakTop: boolean = false;
  public hasDarkOakTop: boolean = false;
  public hasOldOakTop: boolean = false;

  public selectedSize: ProductSizes = ProductSizes.size200x100;
  public selectedLegs: ProductLegs = ProductLegs.black;
  public selectedTop: ProductTops = ProductTops.standard_oak;

  constructor(private productSelectionService: ProductSelectorService) { 
    
  }

  ngOnInit() {
    if(this.groupProduct.series == ProductSeries.heritage){      
      this.showTop = false;
      this.hasSize200x100 = true;
      this.hasSize240x100 = true;
      
      this.hasSilverLegs = true;
      this.hasGoldLegs = true;

      this.selectedLegs = ProductLegs.silver;
    }

    if(this.groupProduct.series == ProductSeries.essence){            
      this.hasSize200x100 = true;
      this.hasSize240x100 = true;

      this.hasBlackLegs = true;
      this.hasSilverLegs = true;
      this.hasGoldLegs = true;

      this.hasNormalOakTop = true;
      this.hasDarkOakTop = true;
      this.hasOldOakTop = true;
      this.selectedLegs = ProductLegs.silver;
    }

    if(this.groupProduct.series == ProductSeries.pure){   
      this.showTop = false;         
      this.hasSize200x100 = true;
      this.hasSize240x100 = true;

      this.hasBlackLegs = true;
      this.hasSilverLegs = true;    
    }

    this.updateProduct();
  }

  updateProduct() {
    var selectedProduct = new Product(this.groupProduct, this.selectedSize, this.selectedLegs, this.selectedTop)  
    this.productSelectionService.selectProduct(selectedProduct);
  }

}
