import { Routes, RouterModule } from '@angular/router';
import { ConceptPageComponent } from './concept-page/concept-page.component';

const CONCEPT_ROUTER: Routes = [
    { 
        path: '',
        component: ConceptPageComponent        
    }
];

export const conceptRouter = RouterModule.forChild(CONCEPT_ROUTER);

