import { NgModule } from '@angular/core';

import { SharedModule } from "../common/shared.module";
import { conceptRouter } from './concept.router';
import { ConceptPageComponent } from './concept-page/concept-page.component';
import { ConceptParallaxComponent } from './concept-parallax/concept-parallax.component';



@NgModule({
  declarations: [         
    ConceptPageComponent,
    ConceptParallaxComponent
     ],
    providers: [],
  imports: [ conceptRouter, SharedModule ]
})

export class ConceptModule {}