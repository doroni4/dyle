import { NgModule } from '@angular/core';

import { SharedModule } from "../common/shared.module";
import { productsRouter } from './products.router';
import { ProductsPageComponent } from './products-page/products-page.component';


@NgModule({
  declarations: [         
        ProductsPageComponent
     ],
    providers: [],
  imports: [ productsRouter, SharedModule ]
})

export class ProductsModule {}