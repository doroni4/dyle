import { Routes, RouterModule } from '@angular/router';
import { ProductResolverService } from "../../../services/product-resolver.service";
import { ProductsPageComponent } from './products-page/products-page.component';

const PRODUCTS_ROUTER: Routes = [
    { 
        path: '',
        component: ProductsPageComponent        
    }
];

export const productsRouter = RouterModule.forChild(PRODUCTS_ROUTER);

