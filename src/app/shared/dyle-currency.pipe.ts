import { Pipe, PipeTransform } from '@angular/core';
import {CurrencyPipe} from "@angular/common";

@Pipe({
    name: 'dyleCurrency'
})

export class DyleCurrencyPipe implements PipeTransform {

  transform(value: number | string, locale?: string): string {
    return new Intl.NumberFormat("de-DE", {
      minimumFractionDigits: 2
    }).format(Number(value));
  }

}
