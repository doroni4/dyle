import * as $ from "jquery";

export class GlobalActionsHelper {

  static setLoading(enable) {
        let body = $('body');
        if (enable) {            
            $('.loader').show();
            $('.page-loader').show();
        } else {
            $('.loader').fadeOut();
            $('.page-loader').delay(350).fadeOut('slow');
        }
  }
}
