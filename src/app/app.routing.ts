import { RouterModule, Routes } from '@angular/router';
import { ProductResolverService } from "../services/product-resolver.service";
import { SeriesResolverService } from '../services/series-resolver.service';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/modules/home/home.module#HomeModule',
    pathMatch: 'full'
  },
  {
    path: 'products',
    loadChildren: 'app/modules/products/products.module#ProductsModule',
    pathMatch: 'full'
  },
  {
    path: 'products/:series/:groupProduct',
    pathMatch: 'full',
    resolve: {
      product: ProductResolverService,
    },
    loadChildren: 'app/modules/configurator/configurator.module#ConfiguratorModule'
  },
  {
    path: 'concept',
    loadChildren: 'app/modules/concept/concept.module#ConceptModule',
    pathMatch: 'full'
  },
  {
    path: 'cooperation',
    loadChildren: 'app/modules/cooperation/cooperation.module#CooperationModule',
    pathMatch: 'full'
  },

  {
    path: 'contact/:series',    
    pathMatch: 'full',
    resolve:{
      series: SeriesResolverService
    },
    loadChildren: 'app/modules/contact/contact.module#ContactModule',
  },

  {
    path: 'contact',    
    pathMatch: 'full',
    loadChildren: 'app/modules/contact/contact.module#ContactModule',
  }
];

export const routing = RouterModule.forRoot(routes);