import { Component, OnInit, AfterViewInit, Inject } from '@angular/core';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { Meta, Title } from '@angular/platform-browser';
import { LOCALE_ID, PLATFORM_ID } from '@angular/core';
import { GlobalActionsHelper } from './shared/global-actions.helper'
import { environment } from '../environments/environment';
import { NgwWowService } from 'ngx-wow';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';

declare var $: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
    constructor(
        @Inject(LOCALE_ID) public currentLocale: string,
        @Inject(PLATFORM_ID) public platformId: Object,
        private router: Router,
        private metaService: Meta,
        private titleService: Title,
        private wowService: NgwWowService) {
        //todo use locale_id for setting meta tags
        if (this.currentLocale == "pl") {
            this.titleService.setTitle('dyle furniture - Polska');
            this.metaService.addTags([
                {
                    name: 'description',
                    content: 'Najwyższa jakość, ręczne wykonanie, prostota i nowoczesny design. To są wartości, którymi ' +
                        'kierujemy się nie tylko produkując meble, ale podejmująć każde, nawet najmiejsze decyzje.'
                },
                {
                    name: 'keywords',
                    content: 'dyle, heritage, pure, prestige, stół, meble, kraków, polska, stoły, dąb'
                },
                {
                    name: 'robots',
                    content: 'index, follow'
                }
            ]);
        }
        else if (this.currentLocale == "de") {
            this.titleService.setTitle('dyle furniture - ein dyle');
        }
        else {
            this.titleService.setTitle('dyle furniture');
            this.metaService.addTags([
                {
                    name: 'description',
                    content: 'Highest quality of furniture materials, handmade crafts, excellent simplicity and tables modern design – those are the values considered upon making each and every product. Even the smallest of decisions in our firm must follow these core values. ' +
                        'kierujemy się nie tylko produkując meble, ale podejmująć każde, nawet najmiejsze decyzje.'
                },
                {
                    name: 'keywords',
                    content: 'dyle, heritage, pure, prestige, table, furniture, krakow, poland, tables, wood, oak, old, design, fashion'
                },
                {
                    name: 'robots',
                    content: 'index, follow'
                }
            ]);
        }

    }
    ngOnInit() {

        this.router.events.subscribe((evt) => {
            if (isPlatformBrowser(this.platformId)) {
                if (evt instanceof NavigationEnd) {
                    GlobalActionsHelper.setLoading(false);
                    this.wowService.init();
                    this.initAnimations();
                    window.scrollTo(0, 0);
                }


                if (evt instanceof NavigationStart) {
                    GlobalActionsHelper.setLoading(true);
                }
            }
        });

    }


    initAnimations() {
        if (isPlatformBrowser(this.platformId)) {
            // var wow = new WOW({
            //     mobile: false
            //     });
            //     wow.init();

            /* ---------------------------------------------- /*
             * Scroll Animation
             /* ---------------------------------------------- */

            $('.section-scroll').bind('click', function (e) {
                var anchor = $(this);
                $('html, body').stop().animate({
                    scrollTop: $(anchor.attr('href')).offset().top - 50
                }, 1000);
                e.preventDefault();
            });

            /* ---------------------------------------------- /*
             * Scroll top
             /* ---------------------------------------------- */

            $(window).scroll(function () {
                if ($(this).scrollTop() > 100) {
                    $('.scroll-up').fadeIn();
                } else {
                    $('.scroll-up').fadeOut();
                }
            });

            $('a[href="#totop"]').click(function () {
                $('html, body').animate({ scrollTop: 0 }, 'slow');
                return false;
            });


            /* ---------------------------------------------- /*
             * Initialization General Scripts for all pages
             /* ---------------------------------------------- */

            var homeSection = $('.home-section'),
                navbar = $('.navbar-custom'),
                navHeight = navbar.height(),
                worksgrid = $('#works-grid'),
                width = Math.max($(window).width(), window.innerWidth),
                mobileTest = false;

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                mobileTest = true;
            }

            this.buildHomeSection(homeSection);
            this.navbarAnimation(navbar, homeSection, navHeight);
            this.navbarSubmenu(width);
            this.hoverDropdown(width, mobileTest);

            var self = this;
            $(window).resize(function () {
                var width = Math.max($(window).width(), window.innerWidth);
                self.buildHomeSection(homeSection);
                self.hoverDropdown(width, mobileTest);
            });

            $(window).scroll(function () {
                self.effectsHomeSection(homeSection, this);
                self.navbarAnimation(navbar, homeSection, navHeight);
            });

            /* ---------------------------------------------- /*
             * Set sections backgrounds
             /* ---------------------------------------------- */

            var module = $('.home-section, .module, .module-small, .side-image');
            module.each(function (i) {
                if ($(this).attr('data-background')) {
                    $(this).css('background-image', 'url(' + $(this).attr('data-background') + ')');
                }
            });

            /* ---------------------------------------------- /*
             * Navbar collapse on click
             /* ---------------------------------------------- */

            $(document).on('click', '.navbar-collapse.in', function (e) {
                if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
                    $(this).collapse('hide');
                }
            });


            /* ---------------------------------------------- /*
             * Video popup, Gallery
             /* ---------------------------------------------- */

            $('.video-pop-up').magnificPopup({
                type: 'iframe'
            });

            $(".gallery-item").magnificPopup({
                delegate: 'a',
                type: 'image',
                gallery: {
                    enabled: true,
                    navigateByImgClick: true,
                    preload: [0, 1]
                },
                image: {
                    titleSrc: 'title',
                    tError: 'The image could not be loaded.'
                }
            });
        }
    }


    /* ---------------------------------------------- /*
     * Home section height
     /* ---------------------------------------------- */

    buildHomeSection(homeSection) {
        if (homeSection.length > 0) {
            if (homeSection.hasClass('home-full-height')) {
                homeSection.height($(window).height());
            } else {
                homeSection.height($(window).height() * 0.85);
            }
        }
    }


    /* ---------------------------------------------- /*
     * Home section effects
     /* ---------------------------------------------- */

    effectsHomeSection(homeSection, scrollTopp) {
        if (homeSection.length > 0) {
            var homeSHeight = homeSection.height();
            var topScroll = $(document).scrollTop();
            if ((homeSection.hasClass('home-parallax')) && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                homeSection.css('top', (topScroll * 0.55));
            }
            if (homeSection.hasClass('home-fade') && ($(scrollTopp).scrollTop() <= homeSHeight)) {
                var caption = $('.caption-content');
                caption.css('opacity', (1 - topScroll / homeSection.height() * 1));
            }
        }
    }

    /* ---------------------------------------------- /*
     * Transparent navbar animation
     /* ---------------------------------------------- */

    navbarAnimation(navbar, homeSection, navHeight) {
        var topScroll = $(window).scrollTop();
        if (navbar.length > 0 && homeSection.length > 0) {
            if (topScroll >= navHeight) {
                navbar.removeClass('navbar-transparent');
            } else {
                navbar.addClass('navbar-transparent');
            }
        }
    }

    /* ---------------------------------------------- /*
     * Navbar submenu
     /* ---------------------------------------------- */

    navbarSubmenu(width) {
        if (width > 767) {
            $('.navbar-custom .navbar-nav > li.dropdown').hover(function () {
                var MenuLeftOffset = $('.dropdown-menu', $(this)).offset().left;
                var Menu1LevelWidth = $('.dropdown-menu', $(this)).width();
                if (width - MenuLeftOffset < Menu1LevelWidth * 2) {
                    $(this).children('.dropdown-menu').addClass('leftauto');
                } else {
                    $(this).children('.dropdown-menu').removeClass('leftauto');
                }
                if ($('.dropdown', $(this)).length > 0) {
                    var Menu2LevelWidth = $('.dropdown-menu', $(this)).width();
                    if (width - MenuLeftOffset - Menu1LevelWidth < Menu2LevelWidth) {
                        $(this).children('.dropdown-menu').addClass('left-side');
                    } else {
                        $(this).children('.dropdown-menu').removeClass('left-side');
                    }
                }
            });
        }
    }

    /* ---------------------------------------------- /*
     * Navbar hover dropdown on desctop
     /* ---------------------------------------------- */

    hoverDropdown(width, mobileTest) {
        if ((width > 767) && (mobileTest !== true)) {
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').removeClass('open');
            var delay = 0;
            var setTimeoutConst;
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').hover(function () {
                var $this = $(this);
                setTimeoutConst = setTimeout(function () {
                    $this.addClass('open');
                    $this.find('.dropdown-toggle').addClass('disabled');
                }, delay);
            },
                function () {
                    clearTimeout(setTimeoutConst);
                    $(this).removeClass('open');
                    $(this).find('.dropdown-toggle').removeClass('disabled');
                });
        } else {
            $('.navbar-custom .navbar-nav > li.dropdown, .navbar-custom li.dropdown > ul > li.dropdown').unbind('mouseenter mouseleave');
            $('.navbar-custom [data-toggle=dropdown]').not('.binded').addClass('binded').on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                $(this).parent().siblings().removeClass('open');
                $(this).parent().siblings().find('[data-toggle=dropdown]').parent().removeClass('open');
                $(this).parent().toggleClass('open');
            });
        }
    }

}
