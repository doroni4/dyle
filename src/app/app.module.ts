import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Response, JsonpModule, URLSearchParams, HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {GlobalActionsHelper} from "./shared/global-actions.helper"
import 'rxjs/add/operator/map';
import { routing } from './app.routing';
import { SharedModule } from "./modules/common/shared.module";
import { NgwWowModule } from "ngx-wow";


@NgModule({
  declarations: [    
    AppComponent
  ],
  imports: [
    HttpModule,
    FormsModule,
    JsonpModule,
    SharedModule,
    BrowserModule.withServerTransition({ appId: 'my-app' }),    
    NgwWowModule.forRoot(),
    routing
  ],  
  bootstrap: [AppComponent]
})
export class AppModule { }
