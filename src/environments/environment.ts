// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  englishAppUrl: 'http://localhost:4200/',
  germanAppUrl: 'http://localhost:4200/contact',
  polishAppUrl: 'http://localhost:4200/product',
  nbpCurrencyApiUrl: 'http://api.nbp.pl/api/exchangerates/rates/A/',

  eurErrorRate: 4.2,
  eurPriceMultiplier: 1.3,

  modelOldOak: {
    basePrice: 21100,
    size240x100Price: 1200,

    silverLegsPrice: 0,
    goldLegsPrice: 0,

    darkOakPrice: 0,
    oldOakPrice: 0,
    altImagesQty: 2
  },

  modelBlackOak: {
    basePrice: 48500,
    size240x100Price: 8000,

    silverLegsPrice: 0,
    goldLegsPrice: 0,

    darkOakPrice: 0,
    oldOakPrice: 0,
    altImagesQty: 2
  },

  model001: {
    basePrice: 16600,
    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 3
  },

  modelStraight: {
    basePrice: 12800,

    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 0
  },
  model003: {
    basePrice: 12800,

    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 3
  },
  modelPropeller: {
    basePrice: 12500,
    size240x100Price: 600,

    silverLegsPrice: 2500,
    goldLegsPrice: 2500,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 3
  },

  modelTheOne: {
    basePrice: 8700,

    size240x100Price: 600,

    silverLegsPrice: 2500,
    goldLegsPrice: 2500,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 0
  },

  modelVegasVic: {
    basePrice: 8700,

    size240x100Price: 600,

    silverLegsPrice: 3000,
    goldLegsPrice: 3000,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 0
  }
};
