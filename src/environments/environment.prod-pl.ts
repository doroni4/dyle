export const environment = {
  production: true,

  englishAppUrl: 'http://dyle-dev.constrade.it',
  germanAppUrl: 'http://dyle-dev.de.constrade.it',
  polishAppUrl: 'http://dyle-dev.pl.constrade.it',
  nbpCurrencyApiUrl: 'http://api.nbp.pl/api/exchangerates/rates/A/',

  eurErrorRate: 4.2,
  eurPriceMultiplier: 1.3,

  modelOldOak: {
    basePrice: 21100,
    size240x100Price: 1200,

    silverLegsPrice: 0,
    goldLegsPrice: 0,

    darkOakPrice: 0,
    oldOakPrice: 0,
    altImagesQty: 2
  },

  modelBlackOak: {
    basePrice: 48500,
    size240x100Price: 8000,

    silverLegsPrice: 0,
    goldLegsPrice: 0,

    darkOakPrice: 0,
    oldOakPrice: 0,
    altImagesQty: 2
  },

  model001: {
    basePrice: 16600,
    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 3
  },

  modelStraight: {
    basePrice: 12800,

    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 0
  },
  model003: {
    basePrice: 12800,

    size240x100Price: 600,

    silverLegsPrice: 1000,
    goldLegsPrice: 1000,

    darkOakPrice: 500,
    oldOakPrice: 3000,
    altImagesQty: 3
  },
  modelPropeller: {
    basePrice: 12500,
    size240x100Price: 600,

    silverLegsPrice: 2500,
    goldLegsPrice: 2500,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 3
  },

  modelTheOne: {
    basePrice: 8700,

    size240x100Price: 600,

    silverLegsPrice: 2500,
    goldLegsPrice: 2500,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 0
  },

  modelVegasVic: {
    basePrice: 8700,

    size240x100Price: 600,

    silverLegsPrice: 3000,
    goldLegsPrice: 3000,

    darkOakPrice: 500,
    oldOakPrice: 0,
    altImagesQty: 0
  }
};
