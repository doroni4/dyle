export enum ProductModel {
    old_oak = 1,
    black_oak = 2,
    model_001 = 3,
    straight = 4,
    model_003 = 5,
    propeller = 6,
    the_one = 7,
    vegas_vic = 8
}