export enum ProductSeries {
    heritage = 1,
    essence = 2,
    pure = 3
}