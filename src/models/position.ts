export class UserPosition {
  constructor(public countryCode: string,
              public city: string,
              public ipAddress: string,
              public latitude: number,
            public longitude: number) {
  }
}
