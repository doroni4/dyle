import { ProductSeries } from "../enums/product-series.enum";
import { ProductLegs } from "../enums/product-legs.enum";
import { ProductSizes } from "../enums/product-sizes.enum";
import { ProductTops } from "../enums/product-tops.enum";
import { ProductModel } from "../enums/product-model.enum";
import { GroupProduct } from "./groupProduct";
import { environment } from '../environments/environment';

export class Product {

  public price: number;
  public altImagesQty: number;
  public model: ProductModel;
  public series: ProductSeries;

  constructor(
    public groupProduct: GroupProduct = new GroupProduct(),
    public size: ProductSizes = ProductSizes.size200x100,
    public legs: ProductLegs = ProductLegs.black,
    public top: ProductTops = ProductTops.standard_oak) {
    if (this.groupProduct == null) {
      return;
    }
    this.model = this.groupProduct.modelName;
    this.series = this.groupProduct.series;
    this.altImagesQty = 0;
    this.calculatePriceInPolishCurrency();
  }

  private calculatePriceInPolishCurrency() {
    this.price = 0;
    var modelConfiguration: any;

    switch (this.model) {
      case ProductModel.old_oak:
        modelConfiguration = environment.modelOldOak
        break;
      case ProductModel.black_oak:
        modelConfiguration = environment.modelBlackOak
        break;
      case ProductModel.model_001:
        modelConfiguration = environment.model001
        break;
      case ProductModel.straight:
        modelConfiguration = environment.modelStraight
        break;
      case ProductModel.model_003:
        modelConfiguration = environment.model003
        break;
      case ProductModel.propeller:
        modelConfiguration = environment.modelPropeller
        break;
      case ProductModel.the_one:
        modelConfiguration = environment.modelTheOne
        break;
      case ProductModel.vegas_vic:
        modelConfiguration = environment.modelVegasVic
        break;
    }

    this.price = modelConfiguration.basePrice
    this.altImagesQty = modelConfiguration.altImagesQty;

    if (this.top == ProductTops.dark_oak) {
      this.price += modelConfiguration.darkOakPrice;
    }
    else if (this.top == ProductTops.old_oak) {
      this.price += modelConfiguration.oldOakPrice;
    }

    if (this.legs == ProductLegs.silver) {
      this.price += modelConfiguration.silverLegsPrice;
    }
    else if (this.legs == ProductLegs.gold) {
      this.price += modelConfiguration.goldLegsPrice;
    }

    if (this.size == ProductSizes.size240x100) {
      if (this.series == ProductSeries.essence && this.top == ProductTops.old_oak) {
        this.price += modelConfiguration.size240x100Price * 2;
      }
      else {
        this.price += modelConfiguration.size240x100Price;
      }
    }

  }
}
