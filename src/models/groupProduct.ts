import { ProductSeries } from "../enums/product-series.enum";
import { ProductModel } from "../enums/product-model.enum";

export class GroupProduct {
    constructor(public series: ProductSeries = ProductSeries.essence, public modelName: ProductModel = ProductModel.model_001) {
    }
  }
  