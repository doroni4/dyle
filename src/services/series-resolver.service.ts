import { Injectable } from '@angular/core';
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProductSeries } from "../enums/product-series.enum";


@Injectable()
export class SeriesResolverService implements Resolve<ProductSeries> {
    constructor(private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<any> | any {   
        let series = ProductSeries.heritage;
        switch(route.params['series']){
            case "essence":
                series = ProductSeries.essence;
                break;
            case "pure":
                series = ProductSeries.pure;
                break;
            default:
                series = ProductSeries.heritage;
                break;
        }
        
        return series               
    }
}