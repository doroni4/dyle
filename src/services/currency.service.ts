import { Injectable } from '@angular/core';
import { Http, Response, JsonpModule, Jsonp, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';

import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';

@Injectable()
export class CurrencyService {

	public currentCurrencyExchangeRate: number;
	constructor(private http: Http) {

	}

	getExchangeRate(currencyCode: string): Observable<any> {
		let exchangeCurrencyUrl = environment.nbpCurrencyApiUrl + currencyCode + '?callback=JSONP_CALLBACK';

		return this.http.request(exchangeCurrencyUrl, { method: 'Get' })
			.map(response => { return response.json(); });

	}
}
