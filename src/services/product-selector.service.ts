import { Injectable, Inject, LOCALE_ID } from '@angular/core';
import { Product } from "../models/product";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { CurrencyService } from "../services/currency.service";
import { environment } from '../environments/environment';

@Injectable()
export class ProductSelectorService {

  private productObs = new BehaviorSubject<Product>(new Product)

  selectedProduct = this.productObs.asObservable();
  constructor(
    @Inject(LOCALE_ID) public currentLocale: string,
    private currencyService: CurrencyService) {

  }



  selectProduct(product: Product) {
    if (this.currentLocale == "pl") {
      this.productObs.next(product)
      return;
    }

    if (this.currencyService.currentCurrencyExchangeRate > 0) {
      product.price = this.calculatePriceInEuro(product.price, this.currencyService.currentCurrencyExchangeRate);
      this.productObs.next(product)
      return;
    }

    this.currencyService.getExchangeRate("EUR").subscribe(
      (data) => {
        this.currencyService.currentCurrencyExchangeRate = data.rates[0].mid;
        product.price = this.calculatePriceInEuro(product.price, this.currencyService.currentCurrencyExchangeRate);
        this.productObs.next(product)
      },
      (error) => {
        product.price = this.calculatePriceInEuro(product.price, environment.eurErrorRate);
        this.productObs.next(product)
      });

  }

  calculatePriceInEuro(price: number, euroRate: number) {
    return Math.ceil((price / euroRate * environment.eurPriceMultiplier) / 100) * 100;
  }
}
