import { Injectable } from '@angular/core';
import { Http, Response, Jsonp, URLSearchParams, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import { UserPosition } from '../models/position'
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';
import GoogleMaps from '@google/maps';
import * as _ from "lodash";

declare var $: any;

const GEOLOCATION_ERRORS = {
	'errors.location.unsupportedBrowser': 'Browser does not support location services',
	'errors.location.permissionDenied': 'You have rejected access to your location',
	'errors.location.positionUnavailable': 'Unable to determine your location',
	'errors.location.timeout': 'Service timeout has been reached'
};

@Injectable()
export class GeolocationService {
	googleMapsClient: any;

	constructor(private jsonp: Jsonp) {
		this.googleMapsClient = GoogleMaps.createClient({
			key: 'AIzaSyC73isY4-_92EA7NdLAuTPt73vHh-mIgdQ'
		});
	}

	getPreferableLanguage() {
		let apiUrl: string = 'http://dyle-dev.pl.constrade.it/language-detection.php/?callback=JSONP_CALLBACK';
		return this.jsonp.request(apiUrl).map(res => res.json()).map(pref => pref.lang);
	}
	findLocation(): Observable<UserPosition> {
		let headers = new Headers({ 'Access-Control-Allow-Origin': '*' });
		let options = new RequestOptions({ headers: headers });

		let apiUrl: string = 'https://freegeoip.net/json/?callback=JSONP_CALLBACK';
		return this.jsonp.get(apiUrl).map(res => res.json()).map(geoData => {
			return new UserPosition(
				geoData.country_code,
				geoData.city,
				geoData.ip,
				geoData.latitude,
				geoData.longitude);
		});


	}
	/**
	 * Obtains the geographic position, in terms of latitude and longitude coordinates, of the device.
	 * @param {Object} [opts] An object literal to specify one or more of the following attributes and desired values:
	 *   - enableHighAccuracy: Specify true to obtain the most accurate position possible, or false to optimize in favor of performance and power consumption.
	 *   - timeout: An Integer value that indicates the time, in milliseconds, allowed for obtaining the position.
	 *              If timeout is Infinity, (the default value) the location request will not time out.
	 *              If timeout is zero (0) or negative, the results depend on the behavior of the location provider.
	 *   - maximumAge: An Integer value indicating the maximum age, in milliseconds, of cached position information.
	 *                 If maximumAge is non-zero, and a cached position that is no older than maximumAge is available, the cached position is used instead of obtaining an updated location.
	 *                 If maximumAge is zero (0), watchPosition always tries to obtain an updated position, even if a cached position is already available.
	 *                 If maximumAge is Infinity, any cached position is used, regardless of its age, and watchPosition only tries to obtain an updated position if no cached position data exists.
	 * @returns {Observable} An observable sequence with the geographical location of the device running the client.
	 */
	public getLocation(opts): Observable<any> {

		return Observable.create(observer => {

			if (window.navigator && window.navigator.geolocation) {
				window.navigator.geolocation.getCurrentPosition(
					(position) => {
						this.googleMapsClient.reverseGeocode({
							latlng: [position.coords.latitude, position.coords.longitude]
						}, function (err, response) {
							if (response.status != 200 || err) {
								observer.next(null);
							}

							var results = response.json.results;
							var countryCode = '';
							if (!_.some(results)) {
								observer.next(null);
							}
							results.every(function (result) {
								var addressComponents = result.address_components;
								var addressComponentWithCountry = _.find(addressComponents, function (addrComp) {
									return _.includes(addrComp.types, 'country');
								})
								if (!addressComponentWithCountry) {
									return true;
								}

								var englishCountryCodes = ['BZ', 'CA','CB','IE','JM','NZ','PH','ZA','TT','GB','US','ZW'];
								var germanCountryCodes = ['AT', 'DE','LI','LU','CH'];								

								countryCode = addressComponentWithCountry.short_name;
								return false;
							});

							console.log("Current Country Code: " + countryCode);

							observer.next(countryCode);
							observer.complete();
						});						
					},
					(error) => {
						switch (error.code) {
							case 1:
								observer.error(GEOLOCATION_ERRORS['errors.location.permissionDenied']);
								break;
							case 2:
								observer.error(GEOLOCATION_ERRORS['errors.location.positionUnavailable']);
								break;
							case 3:
								observer.error(GEOLOCATION_ERRORS['errors.location.timeout']);
								break;
						}
					},
					opts);
			}
			else {
				observer.error(GEOLOCATION_ERRORS['errors.location.unsupportedBrowser']);
			}

		});



	}
}
