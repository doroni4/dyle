import { Injectable } from '@angular/core';
import { GroupProduct} from '../models/groupProduct'
import {
    Router, Resolve,
    ActivatedRouteSnapshot
} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ProductSeries } from "../enums/product-series.enum";
import { ProductModel } from '../enums/product-model.enum';


@Injectable()
export class ProductResolverService implements Resolve<GroupProduct> {
    constructor(private router: Router) { }
    
    resolve(route: ActivatedRouteSnapshot): Observable<any> | any {        
        let series = ProductSeries.heritage;
        switch(route.params['series']){
            case "essence":
                series = ProductSeries.essence;
                break;
            case "pure":
                series = ProductSeries.pure;
                break;
            default:
                series = ProductSeries.heritage;
                break;
        }
        var groupName = <string>route.params['groupProduct']
        var productModel: ProductModel = ProductModel[groupName.replace('-','_')];
        return new GroupProduct(series,productModel);
           
                //this.router.navigate(['/not-found']);
    }
}